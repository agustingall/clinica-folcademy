package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Model.Entities.Paciente;

import java.util.List;

public interface IPacienteService {
    List<PacienteEnteroDto> findAllPacientes();
    Paciente findPacienteById(Integer id);
}
