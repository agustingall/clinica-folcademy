package com.folcademy.clinica.Services.Interfaces;

import com.folcademy.clinica.Model.Entities.Medico;

public interface IMedicoService {

    Medico findMedicoById(Integer id);


}
