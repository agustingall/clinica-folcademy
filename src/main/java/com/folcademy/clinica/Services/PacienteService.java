package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import com.folcademy.clinica.Services.Interfaces.IPacienteService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.folcademy.clinica.Exceptions.ValidationException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PacienteService implements IPacienteService {

    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;
    private final PersonaRepository personaRepository;
    private final PersonaMapper personaMapper;

    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper, PersonaRepository personaRepository, PersonaMapper personaMapper) {

        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
        this.personaRepository = personaRepository;
        this.personaMapper = personaMapper;
    }


    @Override
    public List<PacienteEnteroDto> findAllPacientes() {
        List<PacienteEnteroDto> coleccion =  ((List<Paciente>) pacienteRepository.findAll()).stream().map(pacienteMapper::entityToEnteroDto).collect(Collectors.toList());
        if (coleccion.isEmpty()){
            throw new NotFoundException("Lista vacia");
        }

        return coleccion;
    }

    @Override
    public Paciente findPacienteById(Integer id) {
        if (!pacienteRepository.existsById(id)){
            throw new NotFoundException("Paciente no encontrado");
        }
        return pacienteRepository.findById(id).get();
    }

    public Page<PacienteDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
       /* if (coleccion.isEmpty()){
            throw new NotFoundException("Lista vacia");
        }*/
        return pacienteRepository.findAll(pageable).map(pacienteMapper::entityToDto);
    }

    public PacienteEnteroDto agregar(PacienteEnteroDto entity) {
        int id = entity.getIdpaciente();
        if (id < 0) {
            throw new ValidationException("Id no valido");
        } else {
            if (pacienteRepository.existsById(id)) {
                throw new NotFoundException("Id ya en uso");
            }
            entity.setIdpaciente(null);

            Persona p = personaRepository.save(personaMapper.dtoPacienteToPersona(entity));

            Paciente m = pacienteMapper.enteroDtoToEntity(entity);
            m.setPersona(p);
            pacienteRepository.save(pacienteMapper.enteroDtoToEntity(entity));
            return entity;
        }
    }

    public PacienteEnteroDto editar(Integer idPaciente, PacienteEnteroDto dto) {
        if (!pacienteRepository.existsById(idPaciente))
            throw new NotFoundException("El paciente no existe");
        dto.setIdpaciente(idPaciente);

        Paciente  p = pacienteMapper.enteroDtoToEntity(dto);
        personaRepository.save(personaMapper.dtoPacienteToPersona(dto));
        pacienteRepository.save(p);
        return( dto );

    }

    public boolean eliminar(Integer id) {
        if (id < 0) {
            throw new ValidationException("Id no valido");
        }
        else if (!pacienteRepository.existsById(id))
            throw new NotFoundException("El paciente no existe");

        int dni = pacienteRepository.findById(id).get().getDni();
        pacienteRepository.deleteById(id);
        personaRepository.deleteById(dni);
        return true;

    }
}
