package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import com.folcademy.clinica.Services.Interfaces.ITurnoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class TurnoService implements ITurnoService {
    private final TurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;

    public TurnoService(TurnoRepository turnoRepository, TurnoMapper turnoMapper) {
        this.turnoRepository = turnoRepository;
        this.turnoMapper = turnoMapper;
    }


    @Override
    public List<Turno> findAllTurnos() {
        List coleccion =  (List<Turno>) turnoRepository.findAll();
        if (coleccion.isEmpty()){
            throw new NotFoundException("Lista vacia");
        }
        return coleccion;
    }

    public Page<TurnoDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
       /* if (coleccion.isEmpty()){
            throw new NotFoundException("Lista vacia");
        }*/
        return turnoRepository.findAll(pageable).map(turnoMapper::entityToDto);
    }

    @Override
    public Turno findTurnoById(Integer id) {
        if (!turnoRepository.existsById(id)){
            throw new NotFoundException("Turno no encontrado");
        }
        return turnoRepository.findById(id).get();
    }

    public TurnoDto agregar(TurnoDto entity) {
        int id = entity.getId();
        if (id < 0) {
            throw new ValidationException("Id no valido");
        } else {
            if (turnoRepository.existsById(id)) {
                throw new NotFoundException("Id ya en uso");
            }
            entity.setId(null);
            return turnoMapper.entityToDto(turnoRepository.save(turnoMapper.DtoToEntity(entity)));
        }
    }

    public TurnoDto editar(int idTurno, TurnoDto dto) {
        if (!turnoRepository.existsById(idTurno))
            throw new NotFoundException("El turno no existe");
        dto.setId(idTurno);
        return
                turnoMapper.entityToDto(
                        turnoRepository.save(turnoMapper.DtoToEntity(dto)                        )
                );
    }

    public boolean eliminar(Integer id) {
        if (id < 0) {
            throw new ValidationException("Id no valido");
        }
        if (!turnoRepository.existsById(id))
            throw new NotFoundException("El turno no existe");
        turnoRepository.deleteById(id);
        return true;
    }
}
