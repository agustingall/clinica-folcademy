package com.folcademy.clinica.Services;

import com.folcademy.clinica.Exceptions.ValidationException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRepository;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;



import java.util.List;
import java.util.stream.Collectors;

@Service("medicoService")
public class MedicoService {
    private final MedicoRepository medicoRepository;
    private final MedicoMapper medicoMapper;
    private final PersonaRepository personaRepository;
    private final PersonaMapper personaMapper;

    public MedicoService(MedicoRepository medicoRepository, MedicoMapper medicoMapper, PersonaRepository personaRepository, PersonaMapper personaMapper) {

        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
        this.personaRepository = personaRepository;
        this.personaMapper = personaMapper;
    }

    public List<MedicoDto> listarTodos() {
        List coleccion = ((List<Medico>)medicoRepository.findAll()).stream().map(medicoMapper::entityToDto).collect(Collectors.toList());
        if (coleccion.isEmpty()){
            throw new NotFoundException("Lista vacia");
        }
        return coleccion;
    }

    public Page<MedicoDto> listarTodosByPage(Integer pageNumber, Integer pageSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(orderField));
       /* if (coleccion.isEmpty()){
            throw new NotFoundException("Lista vacia");
        }*/
       return medicoRepository.findAll(pageable).map(medicoMapper::entityToDto);
    }

    public Medico getMedicoById(int id){
        return medicoRepository.findById(id).orElse(null);
    }




    public MedicoDto listarUno(Integer id) {
        if (id < 0){
            throw new ValidationException("Id no valido");
        }
        if (!medicoRepository.existsById(id)){
            throw new NotFoundException("Medico no encontrado");
        }
        return  medicoRepository.findById(id).map(medicoMapper::entityToDto).orElse(null);
    }

    public MedicoEnteroDto agregar(MedicoEnteroDto entity) {
        int id = entity.getId();
        if (id < 0){
            throw new ValidationException("Id no valido");
        }
        else {
            if (medicoRepository.existsById(id)){
                throw new NotFoundException("Id ya en uso");
            }

        }
        entity.setId(null);

        Persona p = personaRepository.save(personaMapper.dtoMedicoToPersona(entity));

        Medico m = medicoMapper.enteroDtoToEntity(entity);
        m.setPersona(p);

        return medicoMapper.entityToEnteroDto(medicoRepository.save(m));
    }

    public MedicoEnteroDto editar(Integer idMedico, MedicoEnteroDto dto) {
        if (!medicoRepository.existsById(idMedico))
            throw new NotFoundException("El medico no existe");

        dto.setId(idMedico);

        Medico  m = medicoMapper.enteroDtoToEntity(dto);
        personaRepository.save(personaMapper.dtoMedicoToPersona(dto));
        medicoRepository.save(m);
        return( dto );
    }

    public boolean eliminar(Integer id) {
        if (!medicoRepository.existsById(id))
            throw new NotFoundException("El medico no existe");
        int dni = medicoRepository.findById(id).get().getDni();
        medicoRepository.deleteById(id);
        personaRepository.deleteById(dni);
        return true;
    }

}
