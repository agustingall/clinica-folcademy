package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MedicoMapper {
    public MedicoDto entityToDto(Medico entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new MedicoDto(
                                ent.getId(),
                                ent.getDni(),
                                ent.getProfesion(),
                                ent.getConsulta()
                        )
                )
                .orElse(new MedicoDto());
    }

    public Medico dtoToEntity(MedicoDto dto) {
        Medico entity = new Medico();
        entity.setId(dto.getId());
        entity.setDni(dto.getDni());
        entity.setProfesion(dto.getProfesion());
        entity.setConsulta(dto.getConsulta());

        return entity;
    }

    public MedicoEnteroDto entityToEnteroDto(Medico entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new MedicoEnteroDto(
                                ent.getId(),
                                ent.getPersona().getNombre(),
                                ent.getPersona().getApellido(),
                                ent.getProfesion(),
                                ent.getPersona().getTelefono(),
                                ent.getDni(),
                                ent.getConsulta()
                        )
                )
                .orElse(new MedicoEnteroDto());
    }

    public Medico enteroDtoToEntity(MedicoEnteroDto dto) {
        Medico entity = new Medico();
        entity.setId(dto.getId());

        entity.setDni(dto.getDni());

        entity.setProfesion(dto.getProfesion());
        entity.setConsulta(dto.getConsulta());


        return entity;
    }
}
