package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Services.MedicoService;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component

public class TurnoMapper {


    private final PacienteService pacienteService;
    private final MedicoService medicoService;

    public TurnoMapper(PacienteService pacienteService, MedicoService medicoService) {
        this.pacienteService = pacienteService;
        this.medicoService = medicoService;
    }

    // TODO
        public TurnoDto entityToDto(Turno entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoDto(
                                ent.getId(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getIdpaciente(),
                                ent.getIdmedico()
                        )
                )
                .orElse(new TurnoDto());
    }


    // TODO
    public Turno DtoToEntity(TurnoDto dto) {
        Turno entity = new Turno();
        entity.setId(dto.getId());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setAtendido(0);
        entity.setIdpaciente(dto.getIdPaciente());
        entity.setIdmedico(dto.getIdMedico());
        return entity;
    }
}