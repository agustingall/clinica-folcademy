package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Persona;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PersonaMapper {

    public Persona dtoPacienteToPersona(PacienteEnteroDto dto) {
        Persona entity = new Persona();
        entity.setNombre(dto.getNombre());
        entity.setApellido(dto.getApellido());
        entity.setDni(dto.getDni());
        entity.setTelefono(dto.getTelefono());
        return entity;
    }
    public Persona PacienteToPersona(Paciente dto) {
        Persona entity = new Persona();
        entity.setNombre(dto.getPersona().getNombre());
        entity.setApellido(dto.getPersona().getApellido());
        entity.setDni(dto.getDni());
        entity.setTelefono(dto.getPersona().getTelefono());
        return entity;
    }

    public Persona MedicoToPersona(Medico dto) {
        Persona entity = new Persona();
        entity.setNombre(dto.getPersona().getNombre());
        entity.setApellido(dto.getPersona().getApellido());
        entity.setDni(dto.getDni());
        entity.setTelefono(dto.getPersona().getTelefono());
        return entity;
    }

    public Persona dtoMedicoToPersona(MedicoEnteroDto dto) {
        Persona entity = new Persona();
        entity.setNombre(dto.getNombre());
        entity.setApellido(dto.getApellido());
        entity.setDni(dto.getDni());
        entity.setTelefono(dto.getTelefono());
        return entity;
    }


}
