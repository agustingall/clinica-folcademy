package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Paciente;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PacienteMapper {
    public PacienteDto entityToDto(Paciente entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteDto(
                                ent.getId(),
                                ent.getPersona().getNombre(),
                                ent.getPersona().getApellido(),
                                ent.getDni()
                        )
                )
                .orElse(new PacienteDto());
    }

    public Paciente dtoToEntity(PacienteDto dto) {
        Paciente entity = new Paciente();
        entity.setId(dto.getIdpaciente());
        entity.getPersona().setNombre(dto.getNombre());
        entity.getPersona().setApellido(dto.getApellido());
        entity.setDni(dto.getDni());

        return entity;
    }
    
    public PacienteEnteroDto entityToEnteroDto(Paciente entity) {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteEnteroDto(
                                ent.getId(),
                                ent.getPersona().getNombre(),
                                ent.getPersona().getApellido(),
                                ent.getDireccion(),
                                ent.getDni(),
                                ent.getPersona().getTelefono()
                        )
                )
                .orElse(new PacienteEnteroDto());
    }

    public Paciente enteroDtoToEntity(PacienteEnteroDto dto) {
        Paciente entity = new Paciente();
        entity.setId(dto.getIdpaciente());

        entity.setDni(dto.getDni());
        entity.setDireccion(dto.getDireccion());

        return entity;
    }
}
