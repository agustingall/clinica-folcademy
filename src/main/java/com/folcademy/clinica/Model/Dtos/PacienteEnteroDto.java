package com.folcademy.clinica.Model.Dtos;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PacienteEnteroDto {
    Integer idpaciente;

    @NotNull
    String nombre;
    String apellido;

    String direccion;
    int dni;
    long telefono;
}
