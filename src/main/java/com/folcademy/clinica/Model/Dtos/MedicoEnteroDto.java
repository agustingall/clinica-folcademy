package com.folcademy.clinica.Model.Dtos;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class MedicoEnteroDto {
    Integer id;
    String nombre = "";
    String apellido = "";
    String profesion = "";
    long telefono = 0;
    int dni;
    int consulta = 0;



}