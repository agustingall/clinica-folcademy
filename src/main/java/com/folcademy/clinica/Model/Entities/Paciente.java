package com.folcademy.clinica.Model.Entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "paciente")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Paciente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpaciente", columnDefinition = "INT(11) UNSIGNED")
    Integer id;

    @Column(name = "dni", columnDefinition = "INT(11) UNSIGNED" )
    int dni = 0;


    @Column(name = "direccion")
    String direccion = "";

    @OneToOne(fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    @JsonBackReference
    @JoinColumn(name = "dni", referencedColumnName = "dni",  columnDefinition = "INT(11) UNSIGNED",  insertable = false, updatable = false)
    private Persona persona;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Medico medico = (Medico) o;

        return Objects.equals(id, medico.id);
    }

    @Override
    public int hashCode() {
        return 47971316;
    }
}

/*
package com.folcademy.clinica.Model.Entities;

        import lombok.Getter;
        import lombok.RequiredArgsConstructor;
        import lombok.Setter;
        import lombok.ToString;


        import org.hibernate.Hibernate;
        import org.hibernate.annotations.NotFound;
        import org.hibernate.annotations.NotFoundAction;

        import javax.persistence.*;
        import java.util.Objects;

@Entity
@Table(name = "paciente")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Paciente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpaciente", columnDefinition = "INT(10) UNSIGNED")
    public Integer idpaciente;
    @Column(name = "dni", columnDefinition = "INT(10) UNSIGNED" )
    int dni = 0;

    @Column(name = "direccion" )
    String direccion = "";

    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "dni", referencedColumnName = "dni", insertable = false, updatable = false)
    private Persona persona;



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Medico medico = (Medico) o;

        return Objects.equals(idpaciente, medico.id);
    }

    @Override
    public int hashCode() {
        return 48512154;
    }

}*/