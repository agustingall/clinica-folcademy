package com.folcademy.clinica.Model.Entities;

import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;


import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "medico")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
public class Medico {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idmedico", columnDefinition = "INT(11) UNSIGNED")
    Integer id;

    @Column(name = "dni", columnDefinition = "INT(11) UNSIGNED" )
    int dni = 0;


    @Column(name = "profesion")
    String profesion = "";

    @Column(name = "consulta")
    int consulta = 0;

    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "dni", referencedColumnName = "dni", insertable = false, updatable = false)
    private Persona persona;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Medico medico = (Medico) o;

        return Objects.equals(id, medico.id);
    }

    @Override
    public int hashCode() {
        return 47971316;
    }
}
