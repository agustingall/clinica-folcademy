package com.folcademy.clinica.Model.Entities;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Table(name = "persona")
@Entity
@Getter
@Setter
public class Persona {
    @Id
    @Column(name = "dni", columnDefinition = "INT(11) UNSIGNED", nullable = false)
    private Integer dni;

    @Column(name = "nombre", length = 50)
    private String nombre;

    @Column(name = "apellido", length = 50)
    private String apellido;

    @Column(name = "telefono", columnDefinition = "LONG(11) UNSIGNED")
    private Long telefono;

    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "dni", referencedColumnName = "dni", insertable = false, updatable = false)
    private Persona persona;


}