package com.folcademy.clinica.Controllers;


import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Services.PacienteService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;

import java.util.List;

@RestController
@RequestMapping("/paciente")
// Endpoint para la url localhost:8080/paciente. Acepta cualquier Método (Post, Get, Put, Delete)
public class PacienteController {

    private final PacienteService pacienteService;

    public PacienteController(PacienteService pacienteService) {
        this.pacienteService = pacienteService;
    }

    // HTTP Get Methods
    //@PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "")
    public ResponseEntity<List<PacienteEnteroDto>> findAll() {
        return ResponseEntity
                .ok()
                .body(
                        pacienteService.findAllPacientes())
                ;
    }
    //@PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<Paciente> findAll(@PathVariable(name = "id") Integer id) {
        return ResponseEntity
                .ok()
                .body(
                        pacienteService.findPacienteById(id))
                ;
    }

    //@PreAuthorize("hasAuthority('get')")
    @GetMapping("/page")
    public ResponseEntity<Page<PacienteDto>> listarTodoByPage(
            @RequestParam(name = "pageNumber", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "1") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue = "id") String orderField
    )
    {
        return ResponseEntity.ok(pacienteService.listarTodosByPage(pageNumber, pageSize, orderField));
    }

    // HTTP Post Methods
    //@PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<PacienteEnteroDto> agregar(@RequestBody @Validated PacienteEnteroDto entity) {
        return ResponseEntity.ok(pacienteService.agregar(entity));
    }

    // HTTP PUT Methods
    //@PreAuthorize("hasAuthority('put')")
    @PutMapping("/{idPaciente}")
    public ResponseEntity<PacienteEnteroDto> editar(@PathVariable(name = "idPaciente") int id,
                                                  @RequestBody PacienteEnteroDto dto) {
        return ResponseEntity.ok(pacienteService.editar(id, dto));
    }


    // HTTP Delete Method
    //@PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{idPaciente}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idPaciente") int id) {
        return ResponseEntity.ok(pacienteService.eliminar(id));
    }
}
