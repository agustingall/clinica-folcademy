package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/medico")
public class MedicoController {
    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }

    // HTTP Get Methods
    //@PreAuthorize("hasAuthority('get')")
    @GetMapping("")
    public ResponseEntity<List<MedicoDto>> listarTodo() {
        return ResponseEntity.ok(medicoService.listarTodos());
    }

    //@PreAuthorize("hasAuthority('get')")
    @GetMapping("/page")
    public ResponseEntity<Page<MedicoDto>> listarTodoByPage(
            @RequestParam(name = "pageNumber", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "1") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue = "id") String orderField
            )
    {
        return ResponseEntity.ok(medicoService.listarTodosByPage(pageNumber, pageSize, orderField));
    }

    //@PreAuthorize("hasAuthority('get')")
    @GetMapping("/{idMedico}")
    public ResponseEntity<MedicoDto> listarUno(@PathVariable(name = "idMedico") int id) {
        return ResponseEntity.ok(medicoService.listarUno(id));
    }
    // HTTP Post Methods
    //@PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<MedicoEnteroDto> agregar(@RequestBody @Validated MedicoEnteroDto entity) {
        return ResponseEntity.ok(medicoService.agregar(entity));
    }
    // HTTP PUT Methods
    //@PreAuthorize("hasAuthority('put')")
    @PutMapping("/{idMedico}")
    public ResponseEntity<MedicoEnteroDto> editar(@PathVariable(name = "idMedico") int id,
                                                  @RequestBody MedicoEnteroDto dto) {
        return ResponseEntity.ok(medicoService.editar(id, dto));
    }
    // HTTP Delete Method
    //@PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{idMedico}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idMedico") int id) {
        return ResponseEntity.ok(medicoService.eliminar(id));
    }


}
