package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/turno")
public class TurnoController {

    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }

    // HTTP Get Methods
    //@PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "")
    public ResponseEntity<List<Turno>> findAll() {
        return ResponseEntity
                .ok()
                .body(
                        turnoService.findAllTurnos())
                ;
    }

    //@PreAuthorize("hasAuthority('get')")
    @GetMapping(value = "/{id}")
    public ResponseEntity<Turno> findAll(@PathVariable(name = "id") Integer id) {
        return ResponseEntity
                .ok()
                .body(
                        turnoService.findTurnoById(id))
                ;
    }

    //@PreAuthorize("hasAuthority('get')")
    @GetMapping("/page")
    public ResponseEntity<Page<TurnoDto>> listarTodoByPage(
            @RequestParam(name = "pageNumber", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "1") Integer pageSize,
            @RequestParam(name = "orderField", defaultValue = "id") String orderField
    )
    {
        return ResponseEntity.ok(turnoService.listarTodosByPage(pageNumber, pageSize, orderField));
    }

    // HTTP Post Methods
    //@PreAuthorize("hasAuthority('post')")
    @PostMapping("")
    public ResponseEntity<TurnoDto> agregar(@RequestBody @Validated TurnoDto entity) {
        return ResponseEntity.ok(turnoService.agregar(entity));
    }

    // HTTP PUT Methods
    //@PreAuthorize("hasAuthority('put')")
    @PutMapping("/{idTurno}")
    public ResponseEntity<TurnoDto> editar(@PathVariable(name = "idTurno") int id,
                                                    @RequestBody TurnoDto dto) {
        return ResponseEntity.ok(turnoService.editar(id, dto));
    }


    // HTTP Delete Method
    //@PreAuthorize("hasAuthority('delete')")
    @DeleteMapping("/{idTurno}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idTurno") int id) {
        return ResponseEntity.ok(turnoService.eliminar(id));
    }
}
